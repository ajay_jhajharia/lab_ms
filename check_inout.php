<?php

/**
 * @author Ajay Jhajharia
 * @copyright 2015
 */
include_once('includes/header.php');
include_once('auth.php');
//include_once('reg_process.php');
//form will select info from database through reg_process and display to users_info.php then there onclick confirm will update db through inout_process.php


if((isset($_REQUEST['msg']))&&$_REQUEST['msg']==data_success){
    $pno=$_REQUEST['id'];
    echo '<div class="success">'; 
    echo "You successfully entered data for $pno";  
    echo "</div>";
}
if((isset($_REQUEST['msg']))&&$_REQUEST['msg']==already_checkedIn){
    $pno=$_GET['id'];
    echo '<div class="error">'; 
    echo 'User is already checked In</br>';
    //see details of user check in 
    echo "<a href=checkinout_info.php?id=$pno>Check details</a>" ;
    echo "</div>";
}
if((isset($_REQUEST['msg']))&&$_REQUEST['msg']==already_checkedOut){
    $pno=$_GET['id'];
    echo '<div class="error">'; 
    echo 'User is already checked Out</br>';
    //see details of user check in 
    echo "<a href=checkinout_info.php?id=$pno>Check details</a>" ;
    echo "</div>";
}
if((isset($_REQUEST['msg']))&&$_REQUEST['msg']==not_checkedIn){
    $pno=$_GET['id'];
    echo '<div class="error">'; 
    echo 'User not checked In </br>';
    echo "</div>";
}

?>


<link rel="stylesheet" href="css/checkinout.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script>$(function() {
         var date = new Date();
         var currentMonth = date.getMonth();
         var currentDate = date.getDate();
         var currentYear = date.getFullYear();
         $("#in_pickdate").datepicker( {
            showOn: "button",buttonImage: "images/calendar.gif",buttonImageOnly: true,
            dateFormat: "d /m/ yy",});
          $("#out_pickdate").datepicker( {
            showOn: "button",buttonImage: "images/calendar.gif",buttonImageOnly: true,
            dateFormat: "d /m/ yy",});
         });</script>
<script src="js/selectaction.js"></script>
<style>.pickdate{
    margin-left:35px;
}</style>

<div class="inoutform" >
<h2 id="heading">Check In/Out</h2>
<form  action="users_info.php" method="post">
<fieldset>
P.No.:</br>
<input type="text" name="pno"/></br>
Action:</br>
<select name="action" class="selection" id="action_sel" onchange="changeon();">
<option value="">Select</option>
<option value="check_in">Check In</option>
<option value="check_out">Check Out</option>
</select></br>
<div class="in">
Type of Duty:</br>
<select name="duty" class="selection">
<option value="pmt">Pmt</option>
<option value="ty">Ty</option>
</select></br>

First Meal:</br>
<select name="first_meal" class="selection" >
<option value="breakfast">Breakfast</option>
<option value="lunch">Lunch</option>
<option value="dinner">Dinner</option>
</select><input type="text" name="in_date"class="pickdate" id="in_pickdate"/></br>
Ex Unit:</br>
<input type="text" name="exunit" /></br> </div>
<div class="out">
To Unit:</br>
<input type="text" name="tounit" /></br>
Last Meal:</br>
<select name="first_meal" class="selection" >
<option value="breakfast">Breakfast</option>
<option value="lunch">Lunch</option>
<option value="dinner">Dinner</option>
</select><input type="text" name="out_date" class="pickdate" id="out_pickdate"/></br>
</div>
<input type="submit" name="inout_submit" value="Go >>"/>
</fieldset>
</form>
</div>

<?php
include_once('includes/footer.php');
?>