<?php

/**
 * @author Ajay Jhajharia
 * @copyright 2015
 */

session_start();
unset($_SESSION['userid']);
unset($_SESSION['username']);
unset($_SESSION['uaccess']);
session_destroy();
//same header fun in auth.php
header("location:../index.php");
?>