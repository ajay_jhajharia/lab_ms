<?php

/**
 * @author Ajay Jhajharia
 * @copyright 2015
 */
//display information
include_once('includes/header.php');
include_once('reg_process.php');
include_once('inout_process.php');

$first_meal=$_POST['first_meal'];
$exunit=$_POST['exunit'];
$duty=$_POST['duty'];
$tounit=$_POST['tounit'];

?>
<?php if($numsrow_affected>0){?>
<div class="basic_form" id="info_pane">
<form action="inout_process.php" method="post">
<fieldset><legend>User information:</legend>
<table>
<tbody>
<tr><th>Name:</th><th><input  type="text" disabled="true" name="name"value="<?php echo "$name";?>"/></th></tr>
<tr><th>Rank:</th><th><input type="text" disabled="true" name="rank"value="<?php echo "$rank";?>" /></th></tr>
<tr><th>P.No.:</th><th><input type="text" name="pno"  value="<?php echo "$pno";?>" /></th></tr>
<tr><th>Dept.:</th><th><input type="text" disabled="true" name="dept"value="<?php echo "$dept";?>" /></th></tr>
<div class="in">
<tr><th>Duty:</th><th><input type="text"  name="duty"value="<?php echo "$duty";?>" /></th></tr>
<tr><th>First Meal:</th><th><input type="text"  name="first_meal"value="<?php echo "$first_meal";?>" /></th></tr>
<tr><th>Ex Unit:</th><th><input type="text"  name="exunit"value="<?php echo "$exunit";?>" /></th></tr>
</div>
<div class="out">
<tr><th>To Unit:</th><th><input type="text"  name="tounit"value="<?php echo "$tounit";?>" /></th></tr>
</div>
<tr><th>Date:</th><th><input type="text" name="date" value="<?php $d=date('y/m/d'); print($d);?>"/></th></tr>
<tr><th>Action:</th><th><input type="text"  name="action" value="<?php echo "$action";?>" /></th></tr>

<tr><th><input type="submit" name="confirm" value="Confirm"/></th><th><input type="submit" name="cancel" value="Cancel"/></th></tr>
</tbody></table>
</fieldset>
</form>
</div>
<?php }
if($numsrow_affected<=0){
        echo "<div class='error'>Oops!User not registered</div>";
        echo "<a href='check_inout.php'>Go Back</a>";
    }
include_once('includes/footer.php');
?> 

