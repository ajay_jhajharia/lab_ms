<?php

/**
 * @author Ajay Jhajharia
 * @copyright 2015
 */

include_once('includes/header.php');
include_once('auth.php');
include_once('reg_process.php');
//display errors if any field is left empty/null string validation
if($errors>0){
    foreach($errors as $error){
        ?>
        <div class="errors">*
        <?php  echo ($error); ?>
        </div>
   <?php }    
}
?>
<link rel="stylesheet" href="css/regform.css"/>
<link rel="stylesheet" href="css/common.css"/>
<script src="js/main.js"></script>
<script> </script>
<!--search form-->

<div class="searchform">
<h4>Search User Info</h4>
<form name="search" action="registration.php" method="post">
<input type="text" name="pno" value="Type P.No. Here" id="pers" onfocus="reset_val()"/>
<input type="submit" name="search" value="GO"/></form>
<?php if($_POST['search']){ 
if($numsrow_affected>0){?>
<div id="search_info">
<fieldset><legend>Information Requested are:</legend>
<table>
<tbody>
<tr><th>Name:</th><th class="info"><?php echo "$name";?></th></tr>
<tr><th>Rank:</th><th class="info"><?php echo "$rank";?></th></tr>
<tr><th>P.No.:</th><th class="info"><?php echo "$pno";?></th></tr>
<tr><th>Dept.:</th><th class="info"><?php echo "$dept";?></th></tr>
</tbody></table>
</fieldset>
</div>
<?php } }?>
</div>
<!--registration form-->
<div class="basic_form" >
<h2>Registration Form</h2>
<form name="regform" action="registration.php" method="post">
<fieldset name="reg" ><legend>Registration Form:</legend>
<table>
<tbody>
<tr>
<td>Rank:</td>
<td><input type="text" name="rank" value="<?php echo $rank; ?>"/></td>
</tr>
<tr>
<td>Name:</td>
<td><input type="text" name="name" value="<?php echo $name; ?>"/></td>
</tr>
<tr>
<td>P.No:</td>
<td><input type="text" name="pno" value="<?php echo $pno; ?>"</td>
</tr>
<tr>
<td>Dept.:</td>
<td><input type="text" name="dept"/></td>
</tr>
<tr>
<td>Ration:</td>
<td>
<select name="ration" class="selection">
<option value="veg">Veg</option>
<option value="nonveg">Non Veg</option></td>
</select>
</tr>
<tr>
<td></td>
<td><input type="submit" name="register" value="Register" /></td>
</tr>
</tbody></table>
</fieldset>
</form>
</div>

<?php
include_once('includes/footer.php');
?>