<?php

/**
 * @author Ajay Jhajharia
 * @copyright 2015
 */
session_start();
error_reporting(0);
include_once('dbconnect.php');


?>
<html>
<head>
<title></title>
<link rel="stylesheet" href="css/common.css" />
<script src="js/jquery-1.8.2.js">
</script>
<script src="js/validate.js"></script>
<script src="js/main.js"></script>

</head>
<body background="images/vd.jpg">
<div class="top_pane">
<a href="home.php">
<table><tr><th><div class="logo">
<img src="images/logo.png"/>
</div></th>
<th><pre style="font-size: 30px;font-style: italic;">Management System</pre>
</th>
<th>
<div class="top_left_logout_pane">
<?php if(isset($_SESSION['userid'])&&($_SESSION['userid']!='')){
  echo("Welcome  ".ucfirst($_SESSION['username'])) ;  ?>
  <a href="includes/logout.php">Logout</a>
  <?php
}?>
</div>
</th></tr></table></a></div>

<!--home page link
<a href="home.php"><img src="images/sign_post.gif" width="20px" height="18px" />Home</a>
 -->